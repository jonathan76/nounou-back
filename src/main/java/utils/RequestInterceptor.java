package utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.nounou.constants.EnumExceptions;
import com.nounou.security.JWTAuthorizationFilter;
import com.nounou.services.LoggerService;

public class RequestInterceptor extends HandlerInterceptorAdapter {

	public static String TOKENUSERNAME;
	@Autowired 
    private static LoggerService _loggerService;	
	private static String _className = "UtilMethods";
	
	private void setUserNameFromToken(@RequestHeader(value="Authorization") final String p_tokenHeader) {
    	
		try {
			final String getUserFromTokenMethodName = "getUserFromToken"; // NOPMD by jonathan on 21/07/2019 18:21
	    	
	    	if (!StringUtils.isEmpty(p_tokenHeader)) {
	    		UsernamePasswordAuthenticationToken object = null;
	    		try {
	    			object = JWTAuthorizationFilter.getAuthenticationFromToken(p_tokenHeader);
	    		}catch(Exception e) {
	    			_loggerService.error("System", _className, EnumExceptions.NULLPOINTER, "Erreur en récupérant le userName du token", getUserFromTokenMethodName);
	    		}
	    		if (object != null) {
	    			final String userName = object.getName();
	    			if (!StringUtils.isEmpty(userName)) {
	    				TOKENUSERNAME = userName;
	    			}else {
	    				TOKENUSERNAME = "System";
	    			}
	    		}
	    	}else {
	    		_loggerService.error("System", _className, EnumExceptions.EMPTYSTRING, "Token vide dans le header", getUserFromTokenMethodName);
	    	}
		}catch(Exception e) {
			_loggerService.error(RequestInterceptor.TOKENUSERNAME, "UtilMethods", EnumExceptions.CATCHEDEXCEPTION, e.getCause().getLocalizedMessage(), "getUserFromToken");
		}
    }
	
	@Override
	public boolean preHandle(
	  HttpServletRequest request,
	  HttpServletResponse response, 
	  Object handler) throws Exception {
	     
	    try {
	    	final String authorizationHeader = request.getHeader("Authorization");
		    if (!StringUtils.isEmpty(authorizationHeader)) {
		    	setUserNameFromToken(authorizationHeader);
		    }
	    }catch(Exception e) {
	    	_loggerService.error("System", "RequestInterceptor", EnumExceptions.CATCHEDEXCEPTION, e.getCause().getLocalizedMessage(), "preHandle");
	    }
	     
	    return true;
	}
}
