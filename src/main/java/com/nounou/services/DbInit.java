package com.nounou.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.nounou.entities.Role;
import com.nounou.entities.TypePerson;
import com.nounou.entities.User;
import com.nounou.interfacesRepositories.IRepoRoles;
import com.nounou.interfacesRepositories.IRepoTypePersons;
import com.nounou.interfacesRepositories.IRepoUsers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

/**
 * DbInit
 */
@Service
public class DbInit implements CommandLineRunner{

    @Autowired
    private IRepoRoles repoRoles;
    @Autowired
    private IRepoTypePersons repoTypePersons;
    @Autowired
    private IRepoUsers repoUsers;

    @Override
    public void run(final String... args) throws Exception {

    	// Roles initialization
        final ArrayList<Role> roles = this.repoRoles.findAll();

        Role roleAdmin = null;
        Role roleNounou = null;
        if (roles.isEmpty()){
            this.repoRoles.deleteAll();

            roleAdmin = new Role("Admin");
            roleNounou = new Role("Nounou");
            final Role roleParent = new Role("User");
            final Role roleGuest = new Role("Guest");

            final List<Role> rolesList = Arrays.asList(roleAdmin, roleGuest, roleNounou, roleParent);       
            
            this.repoRoles.saveAll(rolesList);
        }
        
        // Types persons initialization
        final ArrayList<TypePerson> typePersons = this.repoTypePersons.findAll();

        if (typePersons.isEmpty()){
            this.repoTypePersons.deleteAll();

            final TypePerson typeParent = new TypePerson("Parent");
            final TypePerson typeEnfant = new TypePerson("Enfant");
            final TypePerson typeTuteur = new TypePerson("Tuteur");
            final TypePerson typeMedecin = new TypePerson("Médecin");

            final List<TypePerson> typePersonList = Arrays.asList(typeParent, typeEnfant, typeTuteur, typeMedecin);       
            
            this.repoTypePersons.saveAll(typePersonList);
        }
        
     // users initialization
        final ArrayList<User> users = this.repoUsers.findAll();

        if (users.isEmpty()){
            this.repoUsers.deleteAll();

            // Default admin user
            User userAdmin = null;
            if (roleAdmin != null) {
            	userAdmin = new User("jojo", "$2a$10$bXz1F8T.joBpu6BzWEu05.mbqM9W4bmZFBW2eqXeGGHx8bIBfE5VC", "jonathan.royer67@gmail.com", true, true, roleAdmin);
            }else {
            	userAdmin = new User("jojo", "$2a$10$bXz1F8T.joBpu6BzWEu05.mbqM9W4bmZFBW2eqXeGGHx8bIBfE5VC", "jonathan.royer67@gmail.com", true, true);
            }   
            
            // Default nounou user
            User userNounou = null;
            if (roleNounou != null) {
            	userNounou = new User("marilyne", "mojito66", "marilyne.royer67@gmail.com", true, false, roleNounou);
            }else {
            	userNounou = new User("marilyne", "mojito66", "marilyne.royer@gmail.com", true, false);
            }

            // Complete default list to insert
            List<User> usersList = null;
            if (userAdmin != null || userNounou != null) {
            	usersList = new ArrayList<User>();
            	if (userAdmin != null) {
            		usersList.add(userAdmin);
            	} 
            	if (userNounou != null) {
            		usersList.add(userNounou);
            	}
            }
            
            if (usersList != null && usersList.size() > 0) {
            	this.repoUsers.saveAll(usersList);
            }
        }
    }
}