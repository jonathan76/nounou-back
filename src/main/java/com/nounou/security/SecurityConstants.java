package com.nounou.security;

/**
 * SecurityConstants
 */
public class SecurityConstants {

    public static final String SIGNING_KEY = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String BASIC_PREFIX = "Basic ";
    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_TYPE = "JWT";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String TOKEN_ISSUER = "secure-api";
    public static final String TOKEN_AUDIENCE = "secure-app";
}