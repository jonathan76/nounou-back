package com.nounou.restcontrollers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nounou.constants.EnumExceptions;
import com.nounou.entities.User;
import com.nounou.interfacesRepositories.IRepoUsers;
import com.nounou.services.LoggerService;

import utils.RequestInterceptor;

/**
 * restControllerUsers
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "users")
public class RestControllerUsers {
	
    @Autowired
    private IRepoUsers _repoUsers;   
    @Autowired
    private BCryptPasswordEncoder _passwordEncoder;
    @Autowired 
    private LoggerService _loggerService;
    private static String _className = "restControllerUser";

    @PostMapping(value = "add")
    public User add(final User p_user) {

    	try { 		
    		User returnObject = null;
            if (p_user == null) {
            	this._loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.NULLPOINTER, "Objet p_user null", "add");            
            }else {
            	returnObject = p_user;
            	returnObject.setPassword(_passwordEncoder.encode(p_user.getPassword()));
            	returnObject = this._repoUsers.save(p_user);
            }
            return returnObject;
    	}catch(Exception e) {
    		_loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.CATCHEDEXCEPTION, e.getCause().getLocalizedMessage(), "Add");
    		return null;
    	}
    }
    
    @GetMapping(value = "getUserByToken")  
    public User getUserFromToken(@RequestHeader(value="Authorization") final String p_tokenHeader) {
    	    	
    	try {
    		User returnObject = null;
    		Optional<User> optionUser = _repoUsers.findByUserName(RequestInterceptor.TOKENUSERNAME);
    		if (optionUser.isPresent()) {
    			returnObject = optionUser.get();
    			returnObject.setPassword("");
    		}
    		return returnObject;
    	}catch(Exception e) {
    		_loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.CATCHEDEXCEPTION, e.getCause().getLocalizedMessage(), "getUserFromToken");
    		return null;
    	}
    }
    
    /**
     * 
     * @param p_tokenHeader token to explore
     * @return the user name in token
     */
    @GetMapping(value = "getusernamebytoken")  
    public String getUserNameFromToken() {
    	
    	try {
    		final String userName = RequestInterceptor.TOKENUSERNAME;
    		if (!StringUtils.isEmpty(userName) && userName != "System") {
    			return userName;
    		}
			return null;
    	}catch(Exception ex) {
    		String errorMessage = ex.getCause().getLocalizedMessage();
    		_loggerService.error("System", _className, EnumExceptions.CATCHEDEXCEPTION, errorMessage, "GetUserNameFromToken");
    		return null;
    	}
    }

    /**
     * Endpoint for registering a new user
     * @param p_user 
     */
    @PostMapping(value = "signup")    
    public void signUp(@RequestBody final User p_user){
        
    	try {
    		if (p_user != null) {
        		if(p_user.getRole() != null) {
        	        this.add(p_user);
        		}else {
        			this._loggerService.error("Invité", _className, EnumExceptions.EMPTYSTRING, "Role absent", "signUp");
        		}
        	}else {
        		this._loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.NULLPOINTER, "p_user vide", "signUp");
        	}
    	}catch(Exception e) {
    		this._loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.CATCHEDEXCEPTION, e.getCause().getLocalizedMessage(), "signUp");
    	}
    }

    /**
     * Find and return every User in DB
     * @return List<User>
     */
    @GetMapping("getall")
    public List<User> getAll() {

        try {
        	List<User> users =_repoUsers.findAll(); 
        	users.forEach(user -> {
        		user.setPassword("");
        	});
        	return users;
        }catch(Exception e) {
        	this._loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.CATCHEDEXCEPTION, e.getCause().getLocalizedMessage(), "getAll");
    		return null;
        }
    }

    /**
     * Find and return a User by his id
     * @return List<User>
     */
    @GetMapping("get/{id}")
    public User getById(@PathVariable("id") final int p_id) {

    	try {
    		User returnObject = null;
            if (p_id > 0){
               final Optional<User> optionUser = this._repoUsers.findById(p_id);
               if (optionUser.isPresent()){
            	   returnObject = optionUser.get();
            	   returnObject.setPassword("");
               }else {
            	   this._loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.NULLPOINTER, "optionUser", "getById");
               }
            }else {
            	this._loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.NULLPOINTER, "p_id est vide", "getById");
            }

            return returnObject;
    	}catch(Exception e) {
    		this._loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.CATCHEDEXCEPTION, e.getCause().getLocalizedMessage(), "getById");
    		return null;
    	}
    }

    @PostMapping(value = "update")
    public User update(@RequestBody final User p_user) {

    	try {
    		User returnObject = p_user;
        	if(p_user != null) {
        		this._loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.NULLPOINTER, "p_user est vide", "update");
        	}else {
        		returnObject = this.add(p_user);
        	}
        	
            return returnObject;
    	}catch(Exception e) {
    		this._loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.CATCHEDEXCEPTION, e.getCause().getLocalizedMessage(), "update");
    		return null;
    	}
    }
}