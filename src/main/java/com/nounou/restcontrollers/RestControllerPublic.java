package com.nounou.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nounou.constants.EnumExceptions;
import com.nounou.entities.User;
import com.nounou.interfacesRepositories.IRepoUsers;
import com.nounou.security.AuthenticationManagerImpl;
import com.nounou.security.JWTAuthenticationFilter;
import com.nounou.security.SecurityConstants;
import com.nounou.services.LoggerService;

import utils.JWTToken;

@RestController
@CrossOrigin(origins = "*")
public class RestControllerPublic {

	@Autowired
    private AuthenticationManagerImpl _authImpl;
    @Autowired
    private IRepoUsers _repoUsers;   
    @Autowired 
    private LoggerService _loggerService;
    private static String _className = "restControllerPublic";
    
	/**
     * To login a user and retrieve a JWT token
     * @param p_user contains log in infos
     */
    @PostMapping(value = "sign-in")
    public ResponseEntity<?> signIn(@RequestBody final User p_user) {
    	
    	try {
    		ResponseEntity<JWTToken> returnObject = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        	if (p_user != null) {
        		final JWTAuthenticationFilter filter = new JWTAuthenticationFilter(_authImpl, _repoUsers);
            	final Authentication authentication = this.getAuthentication(p_user, filter);    	
            	if (authentication != null) {
            		SecurityContextHolder.getContext().setAuthentication(authentication);
            		final String token = filter.generateToken(authentication);       
            		final HttpHeaders headers = new HttpHeaders();
            		headers.add(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
            		returnObject = ResponseEntity.ok()
            				.headers(headers)
            				.body(new JWTToken(token));
            	}else {
            		this._loggerService.error("System", _className, EnumExceptions.NULLPOINTER, "Objet authentification null", "sign-in");    		
            	}
        	}else {
        		this._loggerService.error("System", _className, EnumExceptions.NULLPOINTER, "Objet p_user null", "sign-in");
        	} 
        		return returnObject;
    	}catch(Exception ex) {
    		String errorMessage = ex.getCause().getLocalizedMessage();
    		_loggerService.error("System", _className, EnumExceptions.CATCHEDEXCEPTION, errorMessage, "sign-in");
    		return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    	}
    }
    
    /**
     * 
     * @param p_user
     * @return authenticated object
     */
    private Authentication getAuthentication(final User p_user, final JWTAuthenticationFilter p_filter) {
    	
    	Authentication returnObject = null;
    	
    	if (StringUtils.isEmpty(p_user.getUserName()) && StringUtils.isEmpty(p_user.getUserName())) {
    		this._loggerService.error("System", _className, EnumExceptions.EMPTYSTRING, "userName et/ou password vide", "getAuthentication");
    	}else {
    		final Authentication authentication = p_filter.attemptAuthentication(p_user.getUserName(), p_user.getPassword());
    		if (authentication != null) {
    			returnObject = authentication;
    		}
    	}
    	return returnObject;
    }
}
