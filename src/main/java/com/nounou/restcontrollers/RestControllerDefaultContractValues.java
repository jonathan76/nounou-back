package com.nounou.restcontrollers;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nounou.constants.EnumExceptions;
import com.nounou.entities.DefaultContractValues;
import com.nounou.entities.Logs;
import com.nounou.interfacesRepositories.IRepoDefaultContractValues;
import com.nounou.services.LoggerService;

import utils.RequestInterceptor;

/**
 * restControllerNounou
 */
@RestController
@RequestMapping("nounou")
public class RestControllerDefaultContractValues {

	@Autowired
    private IRepoDefaultContractValues _repoContractValues;
	@Autowired 
    private LoggerService _loggerService;
	private static String _className = "restControllerDefaultContractValues"; 
	
	/**
     * Add a new object in DB with date of present day
     * @param p_contractValues Object to add in DB
     * @return The inserted object with its ID
     */
    @PostMapping(value = "adddefaultindemnites")
    public DefaultContractValues updateDefaultIndemnites(@RequestBody final DefaultContractValues p_contractValues) {
    	
    	try {
    		DefaultContractValues returnObject = p_contractValues;
        	
        	if (p_contractValues == null) {
        		_loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.NULLPOINTER, "Object p_indemnite null", "updateDefaultIndemnites");
        	}else {
        		if (p_contractValues.getDateChange() == null)
        			p_contractValues.setDateChange(LocalDateTime.now());
        		returnObject = _repoContractValues.save(p_contractValues);    		
        	}
        	
        	return returnObject;
    	}catch(Exception e) {
    		_loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.CATCHEDEXCEPTION, e.getCause().getLocalizedMessage(), "updateDefaultIndemnites");
    		return null;
    	}
    }
    
    /**
     * 
     * @return the last objet inserted in DB
     */
    @GetMapping(value = "lastdefaultindenite")
    public ResponseEntity<?> getLastDefaultContractValues() {
    	
    	try {
    		return new ResponseEntity<DefaultContractValues>(_repoContractValues.getLastContractValuesByDate(), HttpStatus.ACCEPTED);
    	}catch(Exception ex) {
    		String errorMessage = ex.getCause().getCause().getLocalizedMessage();
    		Logs log = _loggerService.error(RequestInterceptor.TOKENUSERNAME, _className, EnumExceptions.SQLEXCEPTION, errorMessage, "getLastDefaultContractValues");
    		return new ResponseEntity<String>("Une erreur est survenue, merci de contacter votre nounou en communiquant le numéro d'erreur " + log.getId(), HttpStatus.BAD_REQUEST);
    	}
    	
    }
}
