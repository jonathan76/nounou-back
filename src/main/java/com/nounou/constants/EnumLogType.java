package com.nounou.constants;

public enum EnumLogType {
	DEBUG,
	WARN,
	ERROR,
	INFO
}
