package com.nounou.constants;

public enum EnumUserRoles {
    ADMIN,
    USER,
    GUEST,
    NOUNOU
}