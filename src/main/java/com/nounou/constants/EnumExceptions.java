package com.nounou.constants;

public enum EnumExceptions {
	NULLPOINTER,
	EMPTYSTRING,
	SQLEXCEPTION,
	CATCHEDEXCEPTION
}
