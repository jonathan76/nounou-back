package com.nounou.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.nounou.constants.EnumExceptions;
import com.nounou.constants.EnumLogType;

/**
 * 
 * @author jonathan
 *
 */
@Entity
@Table(name = "logs")
public class Logs { 

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String className;
	private String exceptionType;
	private String description;
	private String methodName;
	private String logType;
	private LocalDateTime created;
	private String userName;
	
	public EnumLogType getLogType() {
		return EnumLogType.valueOf(logType);
	}
	public void setLogType(final EnumLogType p_logType) {
		this.logType = p_logType.toString();
	}
	
	// Default constructor
	public Logs() {
		
	}
	
	// Surcharged constructor
	public Logs(final String p_userName,
					final EnumLogType p_logType,
					final String p_className,
					final EnumExceptions p_exceptionType,
					final String p_description,
					final String p_methodName,
					final LocalDateTime p_createdDate) {
		this.logType = p_logType.toString();
		this.className = p_className;
		this.created = p_createdDate;
		this.exceptionType = p_exceptionType.toString();
		this.description = p_description;
		this.methodName = p_methodName;
		this.userName = p_userName;
	}
	public int getId() {
		return id;
	}
	public void setId(final int id) {
		this.id = id;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(final String p_className) {
		this.className = p_className;
	}
	public EnumExceptions getExceptionType() {
		return EnumExceptions.valueOf(exceptionType);
	}
	public void setExceptionType(final EnumExceptions exceptionType) {
		this.exceptionType = exceptionType.toString();
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(final String description) {
		this.description = description;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(final String p_methodName) {
		this.methodName = p_methodName;
	}
	public LocalDateTime getCreated() {
		return created;
	}
	public void setCreated(LocalDateTime created) {
		this.created = created;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
